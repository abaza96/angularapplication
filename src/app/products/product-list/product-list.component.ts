import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";

import { IProduct } from "../product";
import { ProductService } from "../products.service";

// import { ProductService } from '../products.service';

@Component({
  // selector: 'pm-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
  // providers: [ProductService]
})
export class ProductListComponent implements OnInit, OnDestroy {
  private _listFilter!: string;
  errorMessage!: string;
  get listFilter(): string {
    return this._listFilter;
  }
  set listFilter(value: string) {
    this._listFilter = value;
    this.filteredProducts = this.performFilteringProcess(value);
  }
  pageTitle: string = 'Product List';
  imageWidth: number = 50;
  imageMargin: number = 2;
  visible: boolean = false;
  filteredProducts: IProduct[] = [];
  products: IProduct[] = [];
  subscription!: Subscription;

  constructor(private productService: ProductService) {}
  ngOnInit(): void {
    this.listFilter = '';
    this.subscription= this.productService.getProducts().subscribe({
      next: (products) => {
        this.products = products;
        this.filteredProducts = this.products;
      },
      error: (errorCaught) => (this.errorMessage = errorCaught),
    });
  }
  toggleImages(): void {
    this.visible = !this.visible;
  }
  descriptiveButtonText(): string {
    return `${this.visible ? 'Hide' : 'Show'} Image`;
  }
  performFilteringProcess(filter: string): IProduct[] {
    filter = filter.toLowerCase();
    return this.products.filter((product: IProduct) =>
    product.productName.toLowerCase().includes(filter)
    );
  }
  onStarClick(message: string): void {
    this.pageTitle = 'Product List';
    this.pageTitle = `${this.pageTitle}: ${message}`;
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
