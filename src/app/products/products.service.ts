import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";

import { IProduct } from "./product";

@Injectable({
  providedIn: 'root'  /*If you want this Svc to be provided in a specific Component
                        set (providers) Key to the Name of this class in the Component()
                        Decorator, See Product-list.component for more info */
})
export class ProductService {
  private productUri = "api/products/products.json";
  constructor(private http: HttpClient) {}
  getProducts(): Observable<IProduct[]> {
    return this.http.get<IProduct[]>(this.productUri)
    .pipe(
      tap(data => console.log("All", JSON.stringify(data)),
      catchError(this.handleError)
    )
    );
  }

  private handleError(handleError: HttpErrorResponse) {
    let message = handleError.error instanceof ErrorEvent ? //Check the error
        `an Error Occured ${handleError.error.message}` // If error was a 400 Bad Request Error
        : `Server returned code: ${handleError.status}, Message: ${handleError.message}`; // If error was a 500 Internal Server Error
     console.error(message);
    return throwError(message);
  }
}
